//
//  BreedDetailsView.swift
//  Dogs
//
//  Created by Jose Pinto on 24/06/2023.
//

import SwiftUI

struct BreedDetailsView: View {
    
    var viewModel: BreedDetailsViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            name
            group
            origin
            temperament
        }
        .padding()
        .navigationTitle(viewModel.name)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private var name: some View {
        HStack {
            Text("Name:")
            Text(viewModel.name)
        }
    }
    
    private var group: some View {
        HStack {
            Text("Group:")
            Text(viewModel.group)
        }
    }
    
    private var origin: some View {
        HStack {
            Text("Origin:")
            Text(viewModel.origin)
        }
    }
    
    private var temperament: some View {
        HStack(alignment: .firstTextBaseline) {
            Text("Temperament:")
            Text(viewModel.temperament)
        }
    }
}

struct BreedDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        let breed = Breed(id: 1,
                          name: "Affenpinscher",
                          group: "Toy",
                          origin: "Germany, France",
                          temperament: "Stubborn, Curious, Playful, Adventurous, Active, Fun-loving",
                          imageURL: "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg")
        
        BreedDetailsView(viewModel: BreedDetailsViewModel(breed: breed))
    }
}
