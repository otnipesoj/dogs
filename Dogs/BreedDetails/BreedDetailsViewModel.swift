//
//  BreedDetailsViewModel.swift
//  Dogs
//
//  Created by Jose Pinto on 24/06/2023.
//

import Foundation

struct BreedDetailsViewModel {
    
    var name: String { breed.name }
    var group: String { breed.group ?? emptyField }
    var origin: String { breed.origin ?? emptyField }
    var temperament: String { breed.temperament ?? emptyField }
    
    private let breed: Breed
    private let emptyField = "-"
    
    init(breed: Breed) {
        self.breed = breed
    }
}
