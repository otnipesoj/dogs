//
//  BreedCellViewModel.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI
import Combine

class BreedCellViewModel: ObservableObject, Identifiable {
    
    let breed: Breed
    var name: String { breed.name }
    @Published private(set) var image: UIImage?
    
    private let imagesApi: ImagesApiProtocol
    private var defaultImage : UIImage {
        UIImage(systemName: "flag.slash.circle.fill")!
    }
    
    init(breed: Breed, imagesApi: ImagesApiProtocol = ImagesApi()) {
        self.breed = breed
        self.imagesApi = imagesApi
    }
    
    @MainActor
    func fetchBreedImage() async{
        guard let imageURL = breed.imageURL else {
            self.image = defaultImage
            return
        }
        
        let image = try? await imagesApi.downloadImage(from: imageURL)
        
        if let image {
            self.image = image
        } else {
            self.image = defaultImage
        }
    }
}
