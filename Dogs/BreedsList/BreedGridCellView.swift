//
//  BreedGridCellView.swift
//  Dogs
//
//  Created by Jose Pinto on 26/06/2023.
//

import SwiftUI

struct BreedGridCellView: View {
    
    @StateObject var viewModel: BreedCellViewModel
    
    var body: some View {
        VStack(alignment: .center) {
            breedImage
            breedName
        }
        .padding()
    }
    
    private var breedImage: some View {
        Group {
            if let image = viewModel.image {
                Image(uiImage: image)
                    .resizable()
                    .cornerRadius(5)
                    .aspectRatio(contentMode: .fit)
            } else {
                RoundedRectangle(cornerRadius: 5)
                    .fill(Color(.systemGray))
                    .task { await viewModel.fetchBreedImage() }
            }
        }
    }
    
    private var breedName: some View {
        Text(viewModel.name)
            .font(.title2)
            .fontWeight(.semibold)
            .foregroundColor(.primary)
            .scaledToFit()
            .minimumScaleFactor(0.6)
    }
}

struct BreedGridCellView_Previews: PreviewProvider {
    static var previews: some View {
        let breed = Breed(id: 1,
                          name: "Affenpinscher",
                          group: "Toy",
                          origin: "Germany, France",
                          temperament: "Stubborn, Curious, Playful, Adventurous, Active, Fun-loving",
                          imageURL: "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg")
        
        BreedGridCellView(viewModel: BreedCellViewModel(breed: breed))
    }
}
