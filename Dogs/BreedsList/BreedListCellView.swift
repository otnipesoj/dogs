//
//  BreedListCellView.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI

struct BreedListCellView: View {
    
    @StateObject var viewModel: BreedCellViewModel
    
    var body: some View {
        HStack(spacing: 20) {
            breedImage
            breedName
        }
        .padding()
    }
    
    private var breedImage: some View {
        Group {
            if let image = viewModel.image {
                Image(uiImage: image)
                    .resizable()
                    .cornerRadius(5)
                    .aspectRatio(contentMode: .fit)
            } else {
                RoundedRectangle(cornerRadius: 5)
                    .fill(Color(.systemGray))
                    .task { await viewModel.fetchBreedImage() }
            }
        }
        .frame(maxWidth: 80, maxHeight: 80)
    }
    
    private var breedName: some View {
        Text(viewModel.name)
            .font(.title2)
            .fontWeight(.semibold)
            .foregroundColor(.primary)
            .scaledToFit()
            .minimumScaleFactor(0.6)
    }
}

struct BreedListCellView_Previews: PreviewProvider {
    static var previews: some View {
        let breed = Breed(id: 1,
                          name: "Affenpinscher",
                          group: "Toy",
                          origin: "Germany, France",
                          temperament: "Stubborn, Curious, Playful, Adventurous, Active, Fun-loving",
                          imageURL: "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg")
        
        BreedListCellView(viewModel: BreedCellViewModel(breed: breed))
    }
}
