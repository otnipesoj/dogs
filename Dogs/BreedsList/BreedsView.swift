//
//  BreedsView.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI

struct BreedsView: View {
    
    @StateObject var viewModel = BreedsViewModel()
    
    var body: some View {
        NavigationView {
            breedsList
        }
    }
    
    private var breedsList: some View {
        ZStack {
            ScrollView {
                LazyVGrid(columns: viewModel.layoutType.columns, alignment: .leading) {
                    ForEach(viewModel.breeds) { cellViewModel in
                        NavigationLink {
                            BreedDetailsView(viewModel: BreedDetailsViewModel(breed: cellViewModel.breed))
                        } label: {
                            switch viewModel.layoutType {
                            case .list:
                                BreedListCellView(viewModel: cellViewModel)
                                    .task { await viewModel.fetchMoreBreedsIfNeeded(currentBreed: cellViewModel) }
                                
                            case .grid:
                                BreedGridCellView(viewModel: cellViewModel)
                                    .task { await viewModel.fetchMoreBreedsIfNeeded(currentBreed: cellViewModel) }
                            }
                        }
                    }
                }
                .task { await viewModel.fetchBreeds() }
                .navigationTitle("Breeds")
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button {
                            viewModel.toggleLayout()
                        } label: {
                            Image(systemName: viewModel.layoutButtonImageName)
                        }
                    }
                }
            }
            
            if viewModel.isFetchingBreeds {
                ProgressView()
                    .scaleEffect(2)
            }
        }
    }
}

struct BreedsView_Previews: PreviewProvider {
    static var previews: some View {
        BreedsView()
    }
}
