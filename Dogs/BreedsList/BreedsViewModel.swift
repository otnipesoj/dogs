//
//  BreedsViewModel.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI
import Combine

enum LayoutType {
    case list
    case grid
    
    var columns: [GridItem] {
        switch self {
        case .list: return [GridItem(.flexible())]
        case .grid: return [GridItem(.flexible()), GridItem(.flexible())]
        }
    }
}

class BreedsViewModel: ObservableObject {
    
    @Published private(set) var breeds = [BreedCellViewModel]()
    @Published private(set) var isFetchingBreeds = false
    @Published private(set) var fetchedAllBreeds = false
    @Published var layoutType = LayoutType.list
    
    var layoutButtonImageName: String {
        switch layoutType {
        case .list: return "square.grid.2x2"
        case .grid: return "rectangle.grid.1x2"
        }
    }
    
    private var page = 0
    private var limit = 10
    private let dogApi: DogApiProtocol
    
    init(dogApi: DogApiProtocol = DogApi()) {
        self.dogApi = dogApi
    }
    
    @MainActor
    func fetchMoreBreedsIfNeeded(currentBreed breed: BreedCellViewModel?) async {
        guard let breed else {
            await fetchBreeds()
            return
        }
        
        let thresholdIndex = breeds.index(breeds.endIndex, offsetBy: -5)
        let breedIndex = breeds.firstIndex { $0.id == breed.id }
        
        if let breedIndex, breedIndex >= thresholdIndex {
            await fetchBreeds()
        }
    }
    
    @MainActor
    func fetchBreeds() async {
        guard !isFetchingBreeds && !fetchedAllBreeds else { return }
        
        self.isFetchingBreeds = true
        let breeds = await dogApi.fetchBreeds(page: page, limit: limit)
        self.page += 1
        
        if breeds.count < limit {
            self.fetchedAllBreeds = true
        }
        
        self.isFetchingBreeds = false
        let breedsCellViewModels = breeds.map { BreedCellViewModel(breed: $0) }
        self.breeds += breedsCellViewModels
    }
    
    func toggleLayout() {
        layoutType = layoutType == .grid ? .list : .grid
    }
}
