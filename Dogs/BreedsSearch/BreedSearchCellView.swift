//
//  BreedSearchCellView.swift
//  Dogs
//
//  Created by Jose Pinto on 25/06/2023.
//

import SwiftUI

struct BreedSearchCellView: View {
    
    @StateObject var viewModel: BreedSearchCellViewModel
    
    var body: some View {
        VStack(alignment: .leading) {
            name
            group
            origin
        }
    }
    
    private var name: some View {
        HStack {
            Text("Name:")
            Text(viewModel.name)
        }
    }
    
    private var group: some View {
        HStack {
            Text("Group:")
            Text(viewModel.group)
        }
    }
    
    private var origin: some View {
        HStack {
            Text("Origin:")
            Text(viewModel.origin)
        }
    }
}

struct BreedSearchCellView_Previews: PreviewProvider {
    static var previews: some View {
        let breed = Breed(id: 1,
                          name: "Affenpinscher",
                          group: "Toy",
                          origin: "Germany, France",
                          temperament: "Stubborn, Curious, Playful, Adventurous, Active, Fun-loving",
                          imageURL: "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg")
        
        BreedSearchCellView(viewModel: BreedSearchCellViewModel(breed: breed))
    }
}
