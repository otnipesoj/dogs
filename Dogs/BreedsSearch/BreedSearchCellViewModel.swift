//
//  BreedSearchCellViewModel.swift
//  Dogs
//
//  Created by Jose Pinto on 25/06/2023.
//

import Foundation

class BreedSearchCellViewModel: ObservableObject, Identifiable {
    
    let breed: Breed
    
    var name: String { breed.name }
    var group: String { breed.group ?? emptyField }
    var origin: String { breed.origin ?? emptyField }
    
    private let emptyField = "-"
    
    init(breed: Breed) {
        self.breed = breed
    }
}
