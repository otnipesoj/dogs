//
//  BreedsSearchView.swift
//  Dogs
//
//  Created by Jose Pinto on 24/06/2023.
//

import SwiftUI

struct BreedsSearchView: View {
    
    @StateObject var viewModel = BreedsSearchViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.breeds) { cellViewModel in
                NavigationLink {
                    BreedDetailsView(viewModel: BreedDetailsViewModel(breed: cellViewModel.breed))
                } label: {
                    BreedSearchCellView(viewModel: cellViewModel)
                }
            }
            .searchable(text: $viewModel.searchTerm)
            .navigationTitle("Search Breeds")
        }
        .onChange(of: viewModel.searchTerm) { newSearchTerm in
            viewModel.search(newSearchTerm)
        }
    }
}

struct BreedsSearchView_Previews: PreviewProvider {
    static var previews: some View {
        BreedsSearchView()
    }
}
