//
//  BreedsSearchViewModel.swift
//  Dogs
//
//  Created by Jose Pinto on 25/06/2023.
//

import Foundation

class BreedsSearchViewModel: ObservableObject {
    
    @Published private(set) var breeds = [BreedSearchCellViewModel]()
    @Published private(set) var isSearching = false
    @Published var searchTerm: String
    
    private let dogApi: DogApiProtocol
    private var timer: Timer?
    
    init(dogApi: DogApiProtocol = DogApi()) {
        self.dogApi = dogApi
        self.searchTerm = ""
    }
    
    func search(_ searchTerm: String) {
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { [weak self] _ in
            guard let self else { return }
            
            Task {
                await self.fetchSearchResults(searchTerm)
            }
        }
    }
    
    @MainActor
    private func fetchSearchResults(_ searchTerm: String) async {
        guard !isSearching else { return }
        
        self.isSearching = true
        let breeds = await dogApi.searchBreeds(searchTerm: searchTerm)
        self.isSearching = false
        
        let breedSearchCellViewModels = breeds.map { BreedSearchCellViewModel(breed: $0) }
        self.breeds = breedSearchCellViewModels
    }
}
