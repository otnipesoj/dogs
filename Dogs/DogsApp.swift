//
//  DogsApp.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI

@main
struct DogsApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
