//
//  HomeView.swift
//  Dogs
//
//  Created by Jose Pinto on 24/06/2023.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        TabView {
            BreedsView()
                .tabItem {
                    Label("Breeds", systemImage: "list.bullet")
                }
            
            BreedsSearchView()
                .tabItem {
                    Label("Search", systemImage: "magnifyingglass")
                }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
