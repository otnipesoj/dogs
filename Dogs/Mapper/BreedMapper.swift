//
//  BreedMapper.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import Foundation

struct BreedMapper {
    static func mapToModel(from dto: BreedDTO) -> Breed {
        Breed(id: dto.id,
              name: dto.name,
              group: dto.breedGroup,
              origin: dto.origin,
              temperament: dto.temperament,
              imageURL: dto.image?.url)
    }
}
