//
//  Breed.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import Foundation

struct Breed {
    let id: Int
    let name: String
    let group: String?
    let origin: String?
    let temperament: String?
    let imageURL: String?
}
