//
//  BreedDTO.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import Foundation

struct BreedDTO: Decodable {
    let weight: BreedMeasurementDTO
    let height: BreedMeasurementDTO
    let id: Int
    let name: String
    let bredFor: String?
    let breedGroup: String?
    let lifeSpan: String
    let temperament: String?
    let origin: String?
    let referenceImageId: String?
    let image: ImageDTO?
}
