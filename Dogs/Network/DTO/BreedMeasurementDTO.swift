//
//  BreedMeasurementDTO.swift
//  Dogs
//
//  Created by Jose Pinto on 24/06/2023.
//

import Foundation

struct BreedMeasurementDTO: Decodable {
    let imperial: String
    let metric: String
}
