//
//  ImageDTO.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import Foundation

struct ImageDTO: Decodable {
    let id: String
    let width: Int
    let height: Int
    let url: String
}
