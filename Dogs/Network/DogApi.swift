//
//  DogApi.swift
//  Dogs
//
//  Created by Jose Pinto on 14/06/2023.
//

import Foundation

class Endpoint {
    var host: String
    var path: String
    var queryItems: [URLQueryItem]
    
    init(host: String, path: String, queryItems: [URLQueryItem] = []) {
        self.host = host
        self.path = path
        self.queryItems = queryItems
    }
}

extension Endpoint {
    var url: URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = host
        urlComponents.path = path
        if (!queryItems.isEmpty) { urlComponents.queryItems = queryItems }
        
        return urlComponents.url
    }
}

class DogApiEndpoint: Endpoint {
    init(path: String, queryItems: [URLQueryItem] = []) {
        super.init(host: "api.thedogapi.com",
                   path: path,
                   queryItems: queryItems)
    }
}

protocol DogApiProtocol {
    func fetchBreeds(page: Int, limit: Int) async -> [Breed]
    func searchBreeds(searchTerm: String) async -> [Breed]
}

class DogApi: DogApiProtocol {
    
    private enum FetchStatus {
        case fetching(Task<[BreedDTO], Error>)
        case fetched([BreedDTO])
    }

    private let decoder = JSONDecoder()
    private var requests = [URL:FetchStatus]()
    
    init() {
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
    
    func fetchBreeds(page: Int, limit: Int) async -> [Breed] {
        let limitURLQueryItem = URLQueryItem(name: "limit", value: String(limit))
        let pageURLQueryItem = URLQueryItem(name: "page", value: String(page))
        let endpoint = DogApiEndpoint(path: "/v1/breeds", queryItems: [limitURLQueryItem, pageURLQueryItem])
        
        do {
            let breedDTOs = try await fetch(endpoint: endpoint)
            let breeds = breedDTOs.map { BreedMapper.mapToModel(from: $0) }
            return breeds
        } catch {
            print("Fetch breeds error: \(error)")
            return []
        }
    }
    
    func searchBreeds(searchTerm: String) async -> [Breed] {
        let searchTermURLQueryItem = URLQueryItem(name: "q", value: searchTerm)
        let endpoint = DogApiEndpoint(path: "/v1/breeds/search", queryItems: [searchTermURLQueryItem])
        
        do {
            let breedDTOs = try await fetch(endpoint: endpoint)
            let breeds = breedDTOs.map { BreedMapper.mapToModel(from: $0) }
            return breeds
        } catch {
            print("Search breeds error: \(error)")
            return []
        }
    }
    
    private func fetch(endpoint: DogApiEndpoint) async throws -> [BreedDTO] {
        guard let url = endpoint.url else {
            throw NetworkError.invalidURL
        }
        
        if let status = requests[url] {
            switch status {
            case .fetching(let task): return try await task.value
            case .fetched(let result): return result
            }
        }
        
        let task: Task<[BreedDTO], Error> = Task {
            let (data, response) = try await URLSession.shared.data(from: url)
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                throw NetworkError.invalidResponse
            }
            
            do {
                return try decoder.decode([BreedDTO].self, from: data)
            } catch {
                print("Fetch error: \(error)")
                throw NetworkError.invalidData
            }
        }
        
        requests[url] = .fetching(task)
        let result = try await task.value
        requests[url] = .fetched(result)
        
        return result
    }
}
