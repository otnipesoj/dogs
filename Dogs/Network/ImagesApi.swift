//
//  ImagesApi.swift
//  Dogs
//
//  Created by Jose Pinto on 13/06/2023.
//

import SwiftUI

protocol ImagesApiProtocol {
    func downloadImage(from url: String) async throws -> UIImage
}

actor ImagesApi: ImagesApiProtocol {
    
    private enum DownloadStatus {
        case downloading(Task<UIImage, Error>)
        case downloaded(UIImage)
    }
    
    private var images = [URLRequest:DownloadStatus]()
    
    func downloadImage(from url: String) async throws -> UIImage {
        guard let url = URL(string: url) else {
            throw NetworkError.invalidURL
        }
        
        let urlRequest = URLRequest(url: url)
        
        if let status = images[urlRequest] {
            switch status {
            case .downloading(let task): return try await task.value
            case .downloaded(let image): return image
            }
        }
        
        let task: Task<UIImage, Error> = Task {
            let (imageData, _) = try await URLSession.shared.data(for: urlRequest)
            let image = UIImage(data: imageData)!
            return image
        }
        
        images[urlRequest] = .downloading(task)
        let image = try await task.value
        images[urlRequest] = .downloaded(image)
        
        return image
    }
}
