//
//  NetworkError.swift
//  Dogs
//
//  Created by Jose Pinto on 14/06/2023.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case alreadyLoadingPage
    case invalidResponse
    case invalidData
}
